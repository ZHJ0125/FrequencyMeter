# FrequencyMeter
使用 Altium Designer 设计一款频率计

# Project Gallery
> 
> 1. Schematic
>

<div align=center><img width="900" height="600" src="http://47.95.13.239/Study/PCB/Show/119_Show_SchDoc.png"/></div>

>
> 2. PCB Circuit
> 

<div align=center><img width="500" height="350" src="http://47.95.13.239/Study/PCB/Show/121_Show_PcbDoc3.png"/></div>

<div align=center><img width="500" height="350" src="http://47.95.13.239/Study/PCB/Show/120_Show_PcbDoc1.png"/></div>

<div align=center><img width="500" height="350" src="http://47.95.13.239/Study/PCB/Show/122_Show_PcbDoc2.png"/></div>

> 
> 3. Real object
> 

<div align=center><img width="500" height="350" src="http://47.95.13.239/Study/PCB/Show/PCB_Front.jpg"/></div>

<div align=center><img width="500" height="350" src="http://47.95.13.239/Study/PCB/Show/PCB_Back.jpg"/></div>
